namespace :unicorn do

  [:stop, :start, :restart]. each do |action|
    desc 'Performs sv #{action.to_s} on unicorn'
    task action do
      on roles(:app) do
        execute "sv #{action.to_s} unicorn_kir"
      end
    end
  end

end
