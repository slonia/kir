require 'socket'               # Get sockets from stdlib

server = TCPServer.open(2000)  # Socket to listen on port 88
loop {                         # Servers run forever
  client = server.accept       # Wait for a client to connect
  while line = client.gets # Read lines from socket
    open(File.join( File.expand_path(File.dirname(__FILE__)),'../public','1.mp4'), 'a') do |f|
      f.puts line
    end
  end
  client.close                 # Disconnect from the client
}
