require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module Kir
  class Application < Rails::Application
    config.generators do |generate|
      generate.helper false
      generate.assets false
      generate.view_specs false
      generate.test_framework nil
    end
    config.assets.precompile += %w(admin.css admin.js)
    Globalize.fallbacks = {en: [:en, :ru], ru: [:ru, :en]}
    config.i18n.fallbacks = true
  end
end
