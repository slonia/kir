USER = 'berlin'

worker_processes 2

working_directory "/home/#{USER}/kir/current"

listen "/home/#{USER}/kir/current/tmp/sockets/unicorn.kir.sock", :backlog => 64

pid "/home/#{USER}/kir/current/tmp/pids/unicorn.kir.pid"

preload_app true

stderr_path "/home/#{USER}/kir/current/log/unicorn.stderr.log"
stdout_path "/home/#{USER}/kir/current/log/unicorn.stdout.log"


after_fork do |server, worker|
  ActiveRecord::Base.establish_connection
end
