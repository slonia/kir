SimpleNavigation::Configuration.run do |navigation|
  navigation.items do |primary|
    primary.dom_class = 'nav navbar-nav'

    primary.item :apps, 'apps', admin_apps_path
    primary.item :surveys, 'surveys', admin_surveys_path
    primary.item :static_pages, 'static pages', admin_static_pages_path
  end
end
