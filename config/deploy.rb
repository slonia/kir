# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'kir'

set :repo_url, 'git@bitbucket.org:slonia/kir.git'
set :linked_dirs, fetch(:linked_dirs, []) + %w[bin log tmp/pids tmp/cache tmp/sockets public/system]
set :linked_files, %w[config/database.yml]
set :log_level, :info

set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.1.1'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value
