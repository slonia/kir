Kir::Application.routes.draw do

  constraints subdomain: /ru|en/ do
    resources :apps, only: [:index, :show] do
      resources :surveys, only: [:index, :show] do
        resources :variants do
          get :upvote, on: :member
        end
      end
    end

    match 'sitemap' => 'info#sitemap', via: :get
    match 'feedback' => 'feedback#feedback', via: :get
    match 'send_feedback' => 'feedback#send_feedback', via: :post

    constraints :slug => /#{StaticPage.slug.values.join('|')}/ do
      match ':slug' => 'static_pages#show', via: :get
    end

    root 'apps#index'
  end

  root 'info#start', as: :start
  devise_for :users

  namespace :admin do
    resources :apps
    resources :surveys
    resources :static_pages

    match 'video' => 'video#show', via: :get
    root 'apps#index'
  end
end
