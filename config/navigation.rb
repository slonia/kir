SimpleNavigation::Configuration.run do |navigation|
  navigation.items do |primary|
    primary.dom_class = 'nav navbar-nav'

    primary.item :apps, 'apps', apps_path
    primary.item :about, 'about', '/about'
    primary.item :site_map, 'site_map', apps_path
    primary.item :contact, 'contact_us', apps_path
    primary.item :share, 'share', apps_path
  end
end
