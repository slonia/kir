class AppMailer < ActionMailer::Base
  default from: "no-reply@ilya-kolodnik.info"

  def feedback(params)
    if params[:email] && params[:text]
      to = ['ilya.kolodnik@gmail.com', 'kiryl.belasheuski@gmail.com']
      mail(to: to, reply_to: params[:email], subject: 'Site Feedback', body: params[:text])
    end
  end
end
