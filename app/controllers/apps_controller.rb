class AppsController < ApplicationController
  load_and_authorize_resource

  def index
    if request.xhr?
      slides = []
      @apps.each_with_index do |app, i|
        slides << {
          active: (0 == i),
          text: app.title,
          description: app.info,
          image: app.image,
          link: app.download_url,
          id: app.id
        }
      end
      respond_to do |format|
        format.json { render(json: {slides: slides, index: 0})}
      end
    else
      render text: '', layout: true
    end
  end

  def show
    if request.xhr?
      respond_to do |format|
        format.json { render(json: @app.to_json(methods: [:rating, :image, :description]))}
      end
    else
      render text: '', layout: true
    end
  end

end
