class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_locale

  layout :layout_resolver

  helper_method :current_locale, :available_locales

  private

    def layout_resolver
      if devise_controller?
        'admin'
      else
        'application'
      end
    end

    def set_locale
      I18n.locale = request.subdomains.first || I18n.default_locale
    end

    def current_locale
      I18n.locale
    end

    def available_locales
      [:en, :ru]
    end
end
