class StaticPagesController < ApplicationController
  # load_and_authorize_resource

  def show
    @static_page = StaticPage.find_by_slug(params[:slug])
    if request.xhr?
      respond_to do |format|
        format.json { render(json: @static_page.to_json)}
      end
    else
      render text: '', layout: true
    end
  end

end
