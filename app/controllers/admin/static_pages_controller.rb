class Admin::StaticPagesController < Admin::AdminController
  load_and_authorize_resource

  def index
  end

  def new
    available_locales.each{ |locale| @static_page.translations.build(locale: locale) }
  end

  def create
    @static_page = StaticPage.create(static_page_params)
    respond_with @static_page, location: admin_static_pages_path
  end

  def edit
    available_locales.each{ |locale| @static_page.translations.find_or_initialize_by_locale(locale) }
  end

  def update
    @static_page.update_attributes(static_page_params)
    respond_with @static_page, location: admin_static_pages_path
  end

  private

    def static_page_params
      params.require(:static_page).permit(:slug, { translations_attributes: [:id, :locale, :content] })
    end
end
