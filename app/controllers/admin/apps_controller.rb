class Admin::AppsController < Admin::AdminController
  load_and_authorize_resource

  def index
  end

  def new
    available_locales.each{ |locale| @app.translations.build(locale: locale) }
  end

  def create
    @app = App.create(app_params)
    respond_with @app, location: admin_apps_path
  end

  def edit
    available_locales.each{ |locale| @app.translations.find_or_initialize_by_locale(locale) }
  end

  def update
    @app.update_attributes(app_params)
    respond_with @app, location: admin_apps_path
  end

  def destroy
    @app.destroy
    respond_with @app, location: admin_apps_path
  end

  private

    def app_params
      params.require(:app).permit(:background, :download_url, :google_name, { translations_attributes: [:id, :locale, :title, :info] })
    end
end
