class Admin::SurveysController < Admin::AdminController
  load_and_authorize_resource

  def index
  end

  def new
    available_locales.each{ |locale| @survey.translations.build(locale: locale) }
  end

  def create
    @survey = Survey.create(survey_params)
    respond_with @survey, location: admin_surveys_path
  end

  def edit
    available_locales.each{ |locale| @survey.translations.find_or_initialize_by_locale(locale) }
  end

  def update
    @survey.update_attributes(survey_params)
    respond_with @survey, location: admin_surveys_path
  end

  private

    def survey_params
      params.require(:survey).permit(:app_id, :multiple, :active, variants_attributes: [:id, :text, :_destroy], translations_attributes: [:id, :locale, :question] )
    end
end
