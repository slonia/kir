class SurveysController < ApplicationController
  load_and_authorize_resource :app
  load_and_authorize_resource :survey, through: :app
  def index
    respond_to do |format|
      format.json { render(json: @surveys.to_json) }
    end
  end

end
