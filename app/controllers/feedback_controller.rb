class FeedbackController < ApplicationController

  respond_to :html, :json

  def feedback
  end

  def send_feedback
    AppMailer.feedback(params[:feedback]).deliver!
    render json: {status: 'ok'}
  end
end
