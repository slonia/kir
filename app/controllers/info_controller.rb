class InfoController < ApplicationController
  def start
    render text: '', layout: 'start'
  end

  def sitemap
    @apps = App.all
    respond_to do |format|
      format.html
      format.json { render json: @apps.to_json }
    end
  end
end
