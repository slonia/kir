class VariantsController < ApplicationController
  load_and_authorize_resource :app
  load_and_authorize_resource :survey, through: :app
  load_and_authorize_resource :variant, through: :survey, except: :create
  def index
    respond_to do |format|
      format.json { render(json: @variants.to_json) }
    end
  end

  def create
    @variant = Variant.new(survey_id: @survey.id)
    available_locales.each do |locale|
      loc_params = {locale: locale}
      loc_params.merge!({text: variant_params[:text]}) if locale == current_locale
      @variant.translations.build(loc_params)
    end
    @variant.save
    cookies.permanent["add_variant_for_#{@survey.id}"] = true
    respond_to do |format|
      format.json { render(json: @variant.to_json) }
    end
  end

  def upvote
    @variant.upvote
    cookies.permanent["voted_for_#{@survey.id}"] = true
    cookies.permanent["voted_for_variant_#{@variant.id}"] = true
    respond_to do |format|
      format.json { render(json: @variant.to_json) }
    end
  end

  private

    def variant_params
      params.permit(:text)
    end

end
