class Variant < ActiveRecord::Base
  belongs_to :survey

  translates :text, fallbacks_for_empty_translations: true
  accepts_nested_attributes_for :translations

  def upvote
    self.votes += 1
    self.save
  end
end
