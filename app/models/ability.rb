class Ability
  include CanCan::Ability

  def initialize(user)
    if user
      can :manage, :all
    else
      can :read, :all
      can :upvote, Variant
      can :create, Variant
    end
  end
end
