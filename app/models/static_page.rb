class StaticPage < ActiveRecord::Base
  translates :content, fallbacks_for_empty_translations: true

  extend Enumerize

  enumerize :slug, in: [:about]

  validates :slug, presence: true, uniqueness: true

  accepts_nested_attributes_for :translations

end
