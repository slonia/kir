class App < ActiveRecord::Base
  translates :title, :info, fallbacks_for_empty_translations: true
  has_attached_file :background, styles: { medium: '300x300>'}
  validates_attachment_content_type :background, content_type: /\Aimage\/.*\Z/

  has_many :surveys

  accepts_nested_attributes_for :translations

  delegate :rating, :description, to: :google_app, allow_nil: true

  def google_app
    @google_app ||= MarketBot::Android::App.new(self.google_name).update if self.google_name
  end

  def image
    self.background.url
  end
end
