class Survey < ActiveRecord::Base
  default_scope -> { order(:app_id).order(:id) }

  belongs_to :app
  has_many :variants

  translates :question, fallbacks_for_empty_translations: true

  accepts_nested_attributes_for :variants, allow_destroy: true
  accepts_nested_attributes_for :translations

end
