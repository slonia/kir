class AppCtrl
  @$inject = ['$scope', 'App']

  constructor: (@$scope, @App) ->
    @$scope.load_apps = @load_apps
    do @load_apps

  load_apps: (params) =>
    @App.index params, (app) =>
      @$scope.slides = app.slides
      @$scope.index = app.index
angular.module('Kir').controller 'AppCtrl', AppCtrl
