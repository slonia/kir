class DocumentsController
  @$inject = ['$scope', '$location', '$stateParams', '$rootScope', '$resource', '$sce']

  constructor: (@$scope, @$location, @$stateParams, @$rootScope, @$resource, @$sce) ->

    return @$location.path '/' unless ~['about', 'contact'].indexOf @$stateParams.name

    @$rootScope.title = 'About' if @$stateParams.name == 'About'
    @$rootScope.title = 'Contact' if @$stateParams.name == 'Contact'

    Document = @$resource '/:name.:format', { format: 'json' }
    Document.get name: @$stateParams.name, (document) =>
      @$scope.document = @$sce.trustAsHtml(document.content)

angular.module('Kir').controller 'DocumentsController', DocumentsController
