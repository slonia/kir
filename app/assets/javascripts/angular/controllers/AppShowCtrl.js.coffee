class AppShowCtrl
  @$inject = ['$scope', '$stateParams', '$cookieStore', 'App', 'Survey', 'Variant']

  constructor: (@$scope, @$stateParams, @$cookieStore, @App, @Survey, @Variant) ->
    @$scope.load_app = @load_app
    @$scope.load_surveys = @load_surveys
    @$scope.load_variants = @load_variants
    @$scope.upvote = @upvote
    @$scope.can_vote = @can_vote
    @$scope.new_variant = @new_variant
    @$scope.can_add_variant_for = @can_add_variant_for
    @$scope.voted_for_survey = @voted_for_survey
    @$scope.voted_for_variant = @voted_for_variant
    @$scope.can_vote_for_new = @can_vote_for_new
    @$scope.app = null
    @$scope.surveys = []
    do @load_app
    do @load_surveys

  load_app: (params) =>
    defaults =
      id: @$stateParams.id
    params = angular.extend(defaults, params)
    @App.show params, (app) =>
      @$scope.app = app

  load_surveys: (params) =>
    defaults =
      id: @$stateParams.id
    params = angular.extend(defaults, params)
    @Survey.query params, (surveys) =>
      @$scope.surveys = surveys

  load_variants: (survey_id) =>
    params =
      survey_id: survey_id
      app_id: @$stateParams.id
    variants = []
    variants = @Variant.query(params)

  can_vote: (survey, variant) =>
    return false unless variant.id
    if survey.multiple
      @voted_for_variant(variant.id)
    else
      @voted_for_variant(survey.id)

  upvote: (survey_id, variant) =>
    params =
      survey_id: survey_id
      app_id: @$stateParams.id
      id: variant.id
    variant.votes += 1
    @Variant.upvote(params)

  new_variant: () =>
    new @Variant

  voted_for_variant: (id) =>
    !@$cookieStore.get('voted_for_variant_' + id)

  voted_for_survey: (id) =>
    !@$cookieStore.get('voted_for_' + id)

  can_vote_for_new: (survey) =>
    survey.multiple || !@voted_for_survey(survey.id)

  can_add_variant_for: (survey) =>
    survey.editable && !@$cookieStore.get('add_variant_for_' + survey.id) && @can_vote_for_new(survey)

angular.module('Kir').controller 'AppShowCtrl', AppShowCtrl
