class FeedbackController
  @$inject = ['$scope', '$stateParams', 'Feedback']

  constructor: (@$scope, @$stateParams, @Feedback) ->
    @$scope.feedback = new @Feedback

angular.module('Kir').controller 'FeedbackController', FeedbackController
