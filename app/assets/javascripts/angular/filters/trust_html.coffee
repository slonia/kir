class trust_html
  @$inject = ['$sce']

  constructor: (@$sce) ->
    return (text) =>
      @$sce.trustAsHtml(text)

angular.module('Kir').filter 'trust_html', trust_html
