class Feedback
  @$inject = ['$resource']

  constructor: (@$resource) ->
    return @$resource '/send_feedback.:format', { format: 'json' }
angular.module('Kir').factory 'Feedback', Feedback
