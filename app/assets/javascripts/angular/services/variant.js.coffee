class Variant
  @$inject = ['$resource']

  constructor: (@$resource) ->
    return @$resource '/apps/:app_id/surveys/:survey_id/variants/:id.:format', { format: 'json' },
      query:
        method: 'GET'
        isArray: true
        cache: true
      upvote:
        url: '/apps/:app_id/surveys/:survey_id/variants/:id/upvote.:format'
        isArray: false
angular.module('Kir').factory 'Variant', Variant
