class App
  @$inject = ['$resource']

  constructor: (@$resource) ->
    return @$resource '/apps.:format', { format: 'json' },
      index:
        method: 'GET'
        isArray: false
        cache: true
      show:
        method: 'GET'
        url: '/apps/:id.:format'
        isArray: false
        format: 'json'

angular.module('Kir').factory 'App', App
