class Survey
  @$inject = ['$resource']

  constructor: (@$resource) ->
    return @$resource '/apps/:id/surveys/:survey_id.:format', { format: 'json' },
      query:
        method: 'GET'
        isArray: true
        cache: true

angular.module('Kir').factory 'Survey', Survey
