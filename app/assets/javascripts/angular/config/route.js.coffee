class RouteConfig
  DOCUMENT_TEMPLATE = '<article ng-bind-html="document"></article>'
  @$inject = ['$routeProvider', '$stateProvider', '$locationProvider']

  constructor: ( @$routeProvider, @$stateProvider, @$locationProvider) ->
    @$locationProvider.html5Mode true

    @$stateProvider
      .state('apps', url: '/', controller: 'AppCtrl', templateUrl: "/apps.html" )
      .state('app', url: '/apps/:id', controller: 'AppShowCtrl', templateUrl: '/app_show.html')
      .state('sitemap', url: '/sitemap', templateUrl: '/sitemap.html')
      .state('feedback', url: '/feedback', controller: 'FeedbackController', templateUrl: '/feedback.html')
      .state('video', url: '/video', templateUrl: '/video.html')
      .state('document', url: '/:name', controller: 'DocumentsController', template: DOCUMENT_TEMPLATE)
    @$routeProvider
      .otherwise(redirectTo: '/')
      # .when('/apps/:id/surveys', title: 'Survey', controller: 'SurveyCtrl', templateUrl: "/survey.html" )

angular.module('Kir').config RouteConfig

angular.module('Kir').run [
  "$location"
  "$rootScope"
  ($location, $rootScope) ->
    $rootScope.$on "$routeChangeSuccess", (event, current, previous) ->
      $rootScope.title = current.$$route.title
      return
]
