#= require angular
#= require angular-route
#= require angular-resource
#= require angular-ui-router.min
#= require angular-cookies.min
#= require ui-bootstrap-custom-0.10.0.min
#= require ui-bootstrap-custom-tpls-0.10.0.min
#= require_self
#= require_tree ./angular/config
#= require_tree ./angular/controllers
#= require_tree ./angular/services
#= require_tree ./angular/filters

dependencies = [
  'ngRoute'
  'ngResource'
  'ui.bootstrap'
  'ui.router'
  'ngCookies'
]
angular.module 'Kir', dependencies
