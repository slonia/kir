class CreateApps < ActiveRecord::Migration
  def change
    create_table :apps do |t|
      t.string :title
      t.text :info

      t.timestamps
    end
  end
end
