class AddeditableToSurvey < ActiveRecord::Migration
  def change
    add_column :surveys, :editable, :boolean, null: false, default: false, index: true
  end
end
