class AddTranslationToVariant < ActiveRecord::Migration
  def up
    Variant.create_translation_table!({
        text: :text
      }, {
        migrate_data: true
      })
    remove_column :variants, :text
  end

  def down
    add_column :variants, :text, :text
    Variant.drop_translation_table!(migrate_data: true)
  end
end
