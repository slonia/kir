class CreateVariants < ActiveRecord::Migration
  def change
    create_table :variants do |t|
      t.references :survey, index: true
      t.text :text
      t.integer :votes, default: 0, null: false

      t.timestamps
    end
  end
end
