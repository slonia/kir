class AddTranslationsToApps < ActiveRecord::Migration
  def up
    App.create_translation_table!({
        title: :string,
        info: :text
      }, {
        migrate_data: true
      })
    remove_column :apps, :info
    remove_column :apps, :title
  end

  def down
    add_column :apps, :info, :text
    add_column :apps, :title, :string
    App.drop_translation_table!(migrate_data: true)
  end
end
