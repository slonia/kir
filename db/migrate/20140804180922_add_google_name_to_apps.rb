class AddGoogleNameToApps < ActiveRecord::Migration
  def change
    add_column :apps, :google_name, :string
  end
end
