class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.text :question
      t.boolean :multiple, default: false, null: false
      t.boolean :active, default: true, null: false
      t.references :app, index: true

      t.timestamps
    end
  end
end
