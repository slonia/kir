class AddTranslationForSurveys < ActiveRecord::Migration
  def up
    Survey.create_translation_table!({
        question: :text
      }, {
        migrate_data: true
      })
    remove_column :surveys, :question
  end

  def down
    add_column :surveys, :question, :text
    Survey.drop_translation_table!(migrate_data: true)
  end
end
