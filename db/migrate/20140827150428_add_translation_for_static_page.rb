class AddTranslationForStaticPage < ActiveRecord::Migration
  def up
    StaticPage.create_translation_table!({
        content: :text
      }, {
        migrate_data: true
      })
    remove_column :static_pages, :content
  end

  def down
    add_column :static_pages, :content, :text
    StaticPage.drop_translation_table!(migrate_data: true)
  end
end
