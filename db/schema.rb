# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140828132357) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "app_translations", force: true do |t|
    t.integer  "app_id",     null: false
    t.string   "locale",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
    t.text     "info"
  end

  add_index "app_translations", ["app_id"], name: "index_app_translations_on_app_id", using: :btree
  add_index "app_translations", ["locale"], name: "index_app_translations_on_locale", using: :btree

  create_table "apps", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.string   "download_url"
    t.string   "google_name"
  end

  create_table "static_page_translations", force: true do |t|
    t.integer  "static_page_id", null: false
    t.string   "locale",         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "content"
  end

  add_index "static_page_translations", ["locale"], name: "index_static_page_translations_on_locale", using: :btree
  add_index "static_page_translations", ["static_page_id"], name: "index_static_page_translations_on_static_page_id", using: :btree

  create_table "static_pages", force: true do |t|
    t.string   "slug",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "survey_translations", force: true do |t|
    t.integer  "survey_id",  null: false
    t.string   "locale",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "question"
  end

  add_index "survey_translations", ["locale"], name: "index_survey_translations_on_locale", using: :btree
  add_index "survey_translations", ["survey_id"], name: "index_survey_translations_on_survey_id", using: :btree

  create_table "surveys", force: true do |t|
    t.boolean  "multiple",   default: false, null: false
    t.boolean  "active",     default: true,  null: false
    t.integer  "app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "editable",   default: false, null: false
  end

  add_index "surveys", ["app_id"], name: "index_surveys_on_app_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "variant_translations", force: true do |t|
    t.integer  "variant_id", null: false
    t.string   "locale",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "text"
  end

  add_index "variant_translations", ["locale"], name: "index_variant_translations_on_locale", using: :btree
  add_index "variant_translations", ["variant_id"], name: "index_variant_translations_on_variant_id", using: :btree

  create_table "variants", force: true do |t|
    t.integer  "survey_id"
    t.integer  "votes",      default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "variants", ["survey_id"], name: "index_variants_on_survey_id", using: :btree

end
